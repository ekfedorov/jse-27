package ru.ekfedorov.tm.command.user;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.command.AbstractUserCommand;
import ru.ekfedorov.tm.exception.system.NullObjectException;

public final class UserIsAuthCommand extends AbstractUserCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Auth consist.";
    }

    @SneakyThrows
    @Override
    public void execute() {
        if (serviceLocator == null) throw new NullObjectException();
        System.out.println("User auth: " + serviceLocator.getAuthService().isAuth());
    }

    @NotNull
    @Override
    public String name() {
        return "is-auth";
    }

}
