package ru.ekfedorov.tm.command.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.command.AbstractProjectCommand;
import ru.ekfedorov.tm.exception.system.NullObjectException;
import ru.ekfedorov.tm.exception.system.NullProjectException;
import ru.ekfedorov.tm.model.Project;
import ru.ekfedorov.tm.util.TerminalUtil;

import java.util.Optional;

public final class ProjectStartByIndexCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Start project by index.";
    }

    @SneakyThrows
    @Override
    public void execute() {
        if (serviceLocator == null) throw new NullObjectException();
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[START PROJECT]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final Optional<Project> project = serviceLocator.getProjectService().startByIndex(userId, index);
        if (!project.isPresent()) throw new NullProjectException();
    }

    @NotNull
    @Override
    public String name() {
        return "start-project-by-index";
    }

}
