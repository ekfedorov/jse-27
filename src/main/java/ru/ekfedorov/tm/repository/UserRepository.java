package ru.ekfedorov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.ekfedorov.tm.api.repository.IUserRepository;
import ru.ekfedorov.tm.model.User;

import java.util.Optional;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    @Override
    public Optional<User> findByLogin(@NotNull final String login) {
        return list.stream()
                .filter(user -> login.equals(user.getLogin()))
                .findFirst();
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@NotNull final String login) {
        return findByLogin(login).isPresent();
    }

    @Override
    public void removeByLogin(@NotNull final String login) {
        findByLogin(login).map(this::remove);
    }

}
