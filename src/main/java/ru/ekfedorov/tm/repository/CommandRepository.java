package ru.ekfedorov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.repository.ICommandRepository;
import ru.ekfedorov.tm.command.AbstractCommand;

import java.util.*;
import java.util.stream.Collectors;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;

public final class CommandRepository implements ICommandRepository {

    @NotNull
    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @Override
    public void add(@NotNull final AbstractCommand command) {
        @Nullable final String arg = command.arg();
        @Nullable final String name = command.name();
        if (arg != null) arguments.put(arg, command);
        if (name != null) commands.put(name, command);
    }

    @NotNull
    public Collection<AbstractCommand> getArguments() {
        return arguments.values();
    }

    @NotNull
    @Override
    public Collection<String> getCommandArgs() {
        return commands.values().stream()
                .map(AbstractCommand::arg)
                .filter(arg -> !isEmpty(arg))
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByArg(@NotNull final String arg) {
        return commands.get(arg);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByName(@NotNull final String name) {
        return commands.get(name);
    }

    @NotNull
    @Override
    public Collection<String> getCommandName() {
        return commands.values().stream()
                .map(AbstractCommand::name)
                .filter(name -> !isEmpty(name))
                .collect(Collectors.toList());
    }

    @NotNull
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

}
