package ru.ekfedorov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.model.Task;

import java.util.List;
import java.util.Optional;

public interface IProjectTaskService {

    @NotNull
    Optional<Task> bindTaskByProject(
            @Nullable String userId, @Nullable String projectId, @Nullable String taskId
    );

    @NotNull
    List<Task> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    boolean removeProjectById(@Nullable String userId, @Nullable String projectId);

    @NotNull
    Optional<Task> unbindTaskFromProject(@Nullable String userId, @Nullable String taskId);

}
