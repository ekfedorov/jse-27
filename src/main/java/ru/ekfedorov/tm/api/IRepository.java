package ru.ekfedorov.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.ekfedorov.tm.model.AbstractEntity;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public interface IRepository<E extends AbstractEntity> {

    @NotNull
    E add(@NotNull E entity);

    void addAll(@NotNull List<E> entities);

    void clear();

    @NotNull
    List<E> findAll();

    @NotNull
    List<E> findAll(@NotNull Comparator<E> comparator);

    @NotNull
    Optional<E> findOneById(@NotNull String id);

    E remove(@NotNull E entity);

    @NotNull
    Optional<E> removeOneById(@NotNull String id);

}
